# macOS Dtrace scripts

I've been using Dtrace off-and-on to diagnose and debug problems on macOS for some years. Unfortunately, some of the scripts that are included with macOS itself have gone rather stale, or didn't survive the port from Solaris/BSD particularly well. This includes the all-important `dtruss` script for tracing processes' syscalls, which doesn't seem to have changed since Mac OS X 10.5, and where many of its options simply don't work as advertised.

While I was doing some development under contract for Microsoft in their attempt at porting [VFS for Git](https://github.com/microsoft/VFSForGit) to macOS, we needed a passable option for tracing, so I was able to update Mac `dtruss` and fix many of its issues as part of that engagement. The [patches were originally applied on that repo](https://github.com/microsoft/VFSForGit/search?o=desc&q=dtruss&s=committer-date&type=commits) but it's not a good long-term home for them, especially as the Mac port for VFS for Git was stopped when Apple deprecated the APIs used in a subsequent macOS release, so that entire subtree has been removed from recent revisions of the repository.

I'll keep updating dtruss here when I make improvements, and I'll likely end up adding new dtrace scripts or modified versions of existing ones.

<p>- Phil</p>
